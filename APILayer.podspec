Pod::Spec.new do |s|
s.name         = "APILayer"
s.version      = "1.0.7"
s.summary      = "A library to make sending API Requests, caching responses and mocking responses easier"
s.homepage     = "https://github.com/livestyled/ios-playbook/tree/master"
s.license      = { :type => "GNU GPLv3", :file => "licence.txt" }
s.authors            = { "Jonathon Albert" => "Jonathon.albert@livestyled.com", "Ross Patman" => "Ross.patman@livestyled.com", "Mickey Lee" => "Mickey.lee@livestyled.com", "Emal Saifi" => "Emal.saifi@livestyled.com" }
s.platform     = :ios, "11.0"
s.swift_version= "5"
s.source       = { :git => "https://bitbucket.org/J_Albert/api-layer.git", :tag => "#{s.version}" }
s.source_files  = "APILayer/**/*.swift"
s.dependency 'RxSwift'
s.dependency 'RxCocoa'
s.dependency 'RxAtomic'
end
