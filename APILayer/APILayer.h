//
//  APILayer.h
//  APILayer
//
//  Created by apple on 13/09/2019.
//  Copyright © 2019 LiveStyled. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonCrypto.h>

//! Project version number for APILayer.
FOUNDATION_EXPORT double APILayerVersionNumber;

//! Project version string for APILayer.
FOUNDATION_EXPORT const unsigned char APILayerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <APILayer/PublicHeader.h>


