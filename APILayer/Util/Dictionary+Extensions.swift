//
//  DictionaryExtensions.swift
//  ConcertLiveCore
//
//  Created by Nico on 20/01/2016.
//  Copyright © 2016 ConcertLive. All rights reserved.
//
import Foundation
extension Dictionary {
  /**
   Creates a Dictionary with keys and values generated by running
   each [key: value] of self through the mapFunction.
   :param: mapFunction
   :returns: Mapped dictionary
   */
  func map <K, V> (_ map: (Key, Value) -> (K, V)) -> [K: V] {
    var mapped = [K: V]()
    for (key, value) in self {
      let (_key, _value) = map(key, value)
      mapped[_key] = _value
    }
    return mapped
  }
}
