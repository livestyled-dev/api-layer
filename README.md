# APILayer Framework

An iOS Framework for allowing for the easy implementation of network requests and accompanying mocked responses using RxSwift and Swift Codable protocol.

Versioning: XCode 10.3 / 11 (Swift 4+), Cocoapods 1.5.3

### How to setup?

1. Open your Podfile
1. Add `APILayer`
1. Run `pod install`

### Getting Started

## Models
1. "ExampleObject" is the contents of the example response
1. "ExampleObjectItemsResponse" is the object representation of the response itself 
```swift

struct ExampleObject: Codable {
    var title: String?
}

struct ExampleObjectItemsResponse: Codable {
    var success: Bool?
    var items: [ExampleObject]
    let next: String?
}
```

## Protocols
### This is the protocol that the mock and live repositories will conform to in order to define their functions 
```swift
protocol ExampleProviding {
    static func all() -> Observable<ExampleObjectItemsResponse>
}
```

## Example Requester
### This struct is where you specify the endpoint you are calling and setup the creation of the Request
```swift
struct ExampleRequester: Requester {
    static var endpoint: String? = "/examples/examplesEndPoint"
}
extension ExampleRequester {
    static func allExamples() -> URLRequest {
        return RequestCreator.createRequest(withRoot: root(), andEndpoint: endpoint!, httpMethod: .GET)
    }
}
```

### The following repository and mock repository both implement the 'ExampleProviding' protocol above, implementing its method. The live repository will be attemping to retrieve "ExampleObjectItemsResponse" objects as a response from the request defined in your requester, using a stratey implemented in "RemoteDiskCacheDataProviding". The mock repository simply creates some mock data in the format required and returns it.

## Repository
```swift
struct ExampleRepository: RemoteDiskCacheDataProviding, ExampleProviding {
    typealias Cdble = ExampleObjectItemsResponse
    typealias Rqstr = ExampleRequester
}
extension ExampleRepository {
    static func all() -> Observable<ExampleObjectItemsResponse> {
        return ExampleRepository.retrieve(type: Cdble.self, forRequest: Rqstr.allExamples(), strategy: .localAndRemote)
    }
}
```

## Mock Repository
```swift
struct ExampleMockRepository: ExampleProviding {
    static func all() -> Observable<ExampleObjectItemsResponse> {
        var examples: [ExampleObject] = []
        for i in 0...15 {
            let example = ExampleObject(title: "Mock Example: \(i)")
            examples.append(example)
        }
        let response = ExampleObjectItemsResponse(success: true, items: examples, next: nextUrl)
        return Observable.from(optional: response)
    }
}
```

Good luck!
